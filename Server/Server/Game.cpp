#include "Game.h"
#define MAX 100
#define PIPE_DUPLEX_ENIMY TEXT("\\\\.\\pipe\\enimy_pipe") 
#define MAPPED_FILE_NAME TEXT("enemyMappedFile")

OVERLAPPED ovl3;
HANDLE hEvent3;
vector <HANDLE> enemyPipes;
HANDLE sharedMemory;
LPCTSTR memoryPointer;

Game::Game(){
	this->mapWidth = GAME_BOARD_SIZE_X;
	this->mapHeight = GAME_BOARD_SIZE_Y;
}

Game::~Game(){}

// gameBoard Construction
void Game::buildGameboard(){

	for (DWORD posX = 0; posX < mapWidth; posX++) {
		vector<Cell> line;
		for (DWORD posY = 0; posY < mapHeight; posY++) {
			line.push_back(Cell(posX, posY));
		}
		gameMap.push_back(line);
	}

	for (DWORD posX = 0; posX < mapWidth; posX++) {
		for (DWORD posY = 0; posY < mapHeight; posY++) {

			if (posX == 0 || posX == mapHeight -1){
				gameMap[posX][posY].putElementInCell(new Indestructible(posX, posY));
			}
			else if (posY == 0 || posY == mapWidth - 1){
				gameMap[posX][posY].putElementInCell(new Indestructible(posX, posY));
			}
			else if (posX % 2 == 0 && posY % 2 == 0) {// par
				gameMap[posX][posY].putElementInCell(new Indestructible(posX, posY));
			}
			else{
				gameMap[posX][posY].putElementInCell(new Empty(posX, posY));
			}
		}
	}

	// place normal brick
	DWORD destructiveBrick = mapWidth * mapHeight / 3;
	
	if (destructiveBrick <= 0)
		destructiveBrick = 1;

	while (destructiveBrick){
		DWORD posX = generateRandomNumber(mapWidth - 1, 0);
		DWORD posY = generateRandomNumber(mapHeight - 1, 0);

		if (gameMap[posX][posY].getElementInCell()->toString() != L"x"){
			 
			if (posX == 1 && posY == 1){
				cout << "nada" << endl;
			} else{

				Destructive *destructive = new Destructive(posX, posY);
				gameMap[posX][posY].putElementInCell(destructive);
				destructiveBrick--;
			}
		}
	}
}

// Player Management
void Game::addPlayer(DWORD playerID, tstring name, HANDLE clientPipe){

	this->player.push_back(new Player(playerID, name, clientPipe));
}

Player *Game::getPlayer(DWORD playerID){

	return this->player[playerID];
}

// Player movement
tstring Game::movePlayerUp(DWORD playerID){

	if (isOutLimits(player[playerID]->getPosX() - 1, player[playerID]->getPosY())){
		player[playerID]->setPosX(player[playerID]->getPosX() - 1);
		return L"Moved successfully!";
	}
	else{
		return L"You cannot move there!";
	}
}

tstring Game::movePlayerDown(DWORD playerID){

	if (isOutLimits(player[playerID]->getPosX() + 1, player[playerID]->getPosY())){
		player[playerID]->setPosX(player[playerID]->getPosX() + 1);
		return L"Moved successfully!";
	}
	else{
		return L"You cannot move there!";
	}
}

tstring Game::movePlayerRight(DWORD playerID){

	if (isOutLimits(player[playerID]->getPosX(), player[playerID]->getPosY() + 1)){
		player[playerID]->setPosY(player[playerID]->getPosY() + 1);
		return L"Moved successfully!";
	} else{
		return L"You cannot move there!";
	}
}

tstring Game::movePlayerLeft(DWORD playerID){
	
	if (isOutLimits(player[playerID]->getPosX(), player[playerID]->getPosY() - 1)){
		player[playerID]->setPosY(player[playerID]->getPosY() - 1);
		return L"Moved successfully!";
	} else{
		return L"You cannot move there!";
	}
}

BOOL Game::isOutLimits(DWORD posX, DWORD posY){

	if (gameMap[posX][posY].getElementInCell()->toString() == L"x"){
		return false;
	}
	else if (gameMap[posX][posY].getElementInCell()->toString() == L"d"){
		return false;
	}
	else if (gameMap[posX][posY].getElementInCell()->toString() == L"b"){
		return false;
	}
	return true;
}

// Bomb explosion
BOOL Game::isDestructible(DWORD posX, DWORD posY){

	if (gameMap[posX][posY].getElementInCell()->toString() == L"x"){
		return FALSE;
	}
	else {
		return TRUE;
	}
}

void Game::explodeBrick(DWORD posX, DWORD posY){

	// TODO: guardar o elemento anterior
	gameMap[posX][posY].putElementInCell(new Explosion(posX, posY));

	return;
}

void Game::cleanAfterExplosion(vector<DWORD> fireBricks, DWORD playerID){

	for (DWORD i = 0; i < fireBricks.size(); i++){

		DWORD posX = fireBricks.at(i);
		DWORD posY = fireBricks.at(i+1);

		player[playerID]->setPoints(player[playerID]->getPoints() + 2);
		gameMap[posX][posY].putElementInCell(new Empty(posX, posY));
		i++;
	}
}

void Game::placeBomb(DWORD playerID){

	//Bomb *bomb = new Bomb(this, player[playerID]);

	gameMap[player[playerID]->getPosX()][player[playerID]->getPosY()].putElementInCell(new Bomb(this, playerID, player[playerID]->getPosX(), player[playerID]->getPosY(), player[playerID]->getBombPower()));
}

// Enimies

void Game::createEnimy(){

	TCHAR executable[MAX] = TEXT("D:/Cloud.ru/Repositories/BombermanSO2/Enimy/Debug/Enimy.exe 2000");
	HANDLE enemyPipe;

	PROCESS_INFORMATION pi; //Windows PID
	STARTUPINFO si;
	
	hEvent3 = CreateEvent(NULL, FALSE, FALSE, NULL);
	ovl3.hEvent = hEvent3;

	ZeroMemory(&si, sizeof(STARTUPINFO));
	si.cb = sizeof(STARTUPINFO);
	_tprintf(TEXT("Process being lunched:%s\n"), executable);
	
	for (int i = 0; i < 2; i++){

		if (CreateProcess(NULL, executable, NULL, NULL, 0, CREATE_NEW_CONSOLE, NULL, NULL, &si, &pi))
			_tprintf(TEXT("Sucess\n"));
		else
			_tprintf(TEXT("Error\n"));

		enemyPipe = CreateNamedPipe(PIPE_DUPLEX_ENIMY, PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED, PIPE_WAIT | PIPE_TYPE_MESSAGE
			| PIPE_READMODE_MESSAGE, MAX, 256, 256, 1000, NULL);

		if (enemyPipe == INVALID_HANDLE_VALUE){
			_tperror(TEXT("Error on enemy connection!"));
			exit(-1);
		}

		ConnectNamedPipe(enemyPipe, &ovl3);
		enemyPipes.push_back(enemyPipe);

	}
	WaitForSingleObject(ovl3.hEvent, INFINITE);
}

void Game::writeMapOnMappedFile(){

	sharedMemory = CreateFileMapping((HANDLE)0xFFFFFFFF, NULL, PAGE_READWRITE, 0, sizeof(packMapToSend()), MAPPED_FILE_NAME);
	tstring *gameM;
	
	if (sharedMemory == NULL){
		_tprintf(TEXT("[Error] On shared memory creation (%d)\n"), GetLastError());
		return;
	}

	gameM = (tstring*) MapViewOfFile(sharedMemory, FILE_MAP_WRITE, 0, 0, sizeof(500));

	if (memoryPointer == NULL){
		_tprintf(TEXT("[Erro]Mapeamento da mem�ria partilhada(%d)\n"), GetLastError());
		return;
	}

	for (int i = 0; i < sizeof(500); i++){
		gameM += i;
	}
}

// Communication
BOOL WINAPI Game::writeMapOnPipe(){

	//writeMapOnMappedFile();

	for (DWORD i = 1; player.size(); i++){
		WriteFile(player[i]->getPipe(), packMapToSend().c_str(), _tcslen(packMapToSend().c_str())*sizeof(TCHAR), NULL, NULL);
	}
	return TRUE;
}

tstring Game::packMapToSend(){

	tstring mapPacked;
	BOOL isPlayer = FALSE;

	for (DWORD i = 0; i < mapWidth; i++) {	
		for (DWORD j = 0; j < mapHeight; j++) {
			for (DWORD k = 0; k < this->player.size(); k++){
				if (i == player[k]->getPosX() && j == player[k]->getPosY()){
					mapPacked += player[k]->toString();
					isPlayer = TRUE;
				}
			}
			if(!isPlayer){
				mapPacked += gameMap[i][j].getElementInCell()->toString();
			}
			isPlayer = FALSE;
		}
		mapPacked += TEXT("\n");
	}

	return mapPacked;
}

void Game::printGameBoard(){

	tcout << std::endl;
	BOOL isPlayer = FALSE;

	for (DWORD i = 0; i < mapWidth; i++) {						
		for (DWORD j = 0; j < mapHeight; j++) {
			for (DWORD k = 0; k < this->player.size(); k++){
				if (i == player[k]->getPosX() && j == player[k]->getPosY()){
					tcout <<  player[k]->toString();
					isPlayer = TRUE;
				}
			}
			if (!isPlayer){
				tcout << gameMap[i][j].getElementInCell()->toString();
			}
			isPlayer = FALSE;
		}
		tcout << std::endl;
	}
}

/// GETTERS e SETTERS
void Game::setGameStarted(BOOL state){
	this->isGameStarted = state;
}

BOOL Game::getGameStarted(){
	return isGameStarted;
}

void Game::setGameMapWidth(DWORD width){
	this->mapWidth = width;
}

DWORD Game::getGameMapWidth()const{
	return mapWidth;
}

void Game::setGameMapHeight(DWORD height){
	this->mapHeight = height;
}

DWORD Game::getGameMapHeight()const{
	return mapHeight;
}

DWORD Game::generateRandomNumber(DWORD max, DWORD min){
	return min + rand() % (max - min + 1);
}

vector<vector<Cell>> Game::getGameMap(){
	return this->gameMap;
}
