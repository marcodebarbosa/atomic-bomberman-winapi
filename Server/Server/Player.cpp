#include "Player.h"

Player::Player(DWORD id,  tstring name, HANDLE pipe){
	
	this->id = id;
	this->name = name;
	this->lives = 3;
	this->points = 0;
	this->posX = 1;
	this->posY = 1;
	this->bombPower = 3;
	this->isBombActive = FALSE;
	this->pipe = pipe;
}

Player::~Player(){
}

DWORD Player::getPosX(){
	return this->posX;
}
DWORD Player::getPosY(){
	return this->posY;
}
void Player::setPosX(DWORD posX){
	this->posX = posX;
}
void Player::setPosY(DWORD posY){
	this->posY = posY;
}

void Player::setName(tstring name){
	this->name = name;
}
tstring Player::getName(){
	return this->name;
}

void Player::setLives(DWORD lives){
	this->lives = lives;
}

DWORD Player::getLives()const{
	return lives;
}

void Player::setBombPower(DWORD power){
	this->bombPower = power;
}
DWORD Player::getBombPower(){
	return this->bombPower;
}

void Player::setPoints(DWORD points){
	this->points = points;
}
DWORD Player::getPoints(){
	return this->points;
}

void Player::setPipe(HANDLE playerPipe){
	this->pipe = playerPipe;
}

HANDLE Player::getPipe(){
	return this->pipe;
}

void Player::setIsBombActive(BOOL option){
	this->isBombActive = option;
}

BOOL Player::getIsBombActive(){
	return this->isBombActive;
}

tstring Player::toString(){
	return L"p";
}