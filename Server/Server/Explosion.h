#pragma once
#include "Element.h"
class Explosion : public Element
{
public:
	Explosion(DWORD posX, DWORD posY);
	~Explosion();

	virtual tstring toString();
};