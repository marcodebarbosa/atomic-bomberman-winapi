#include "Util.h"
#include "Element.h"

class Cell{
	DWORD posX;
	DWORD posY;
	Element *cellElement;

public:
	Cell(DWORD Y = 0, DWORD X = 0);
	~Cell();
	DWORD getCellX();
	DWORD getCellY();

	void putElementInCell(Element *newElement);
	Element *getElementInCell();
};