#pragma once
#include "Util.h"
#define MAX_THREADS 4
#define PIPE_DUPLEX TEXT("\\\\.\\pipe\\server_pipe")
#define PIPE_UPDATE TEXT("\\\\.\\pipe\\server_update") 

class Communication
{

private:

	//static mais info

	DWORD updateThreadID[MAX_THREADS];
	DWORD threadCounter = 0;
	HWND window;

public:
	Communication();
	~Communication();

	void Connection();
	static vector<vector<tstring>> gameMap;

	void setWindowHandle(HWND handle);
	HWND getWindowHandle();

	vector<vector<tstring>> getMap();
	void setGameMap(vector<vector<tstring>> mapAux);

	void sendToServer(tstring msg);
	void receiveFromServer();
};

