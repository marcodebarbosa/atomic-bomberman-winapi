#include "Communication.h"

// Functions
DWORD WINAPI updateMapThread(LPVOID param);

//Global Variables
HANDLE serverDuplexPipe;
HANDLE serverUpdatePipe;
DWORD bytesWritten;
DWORD bytesRead;
vector<vector<tstring>> Communication::gameMap;

Communication::Communication()
{

#ifdef UNICODE 
	_setmode(_fileno(stdin), _O_WTEXT);
	_setmode(_fileno(stdout), _O_WTEXT);
#endif

	for (DWORD i = 0; i < GAME_BOARD_SIZE_X; i++)
	{
		vector<tstring> line;
		for (DWORD j = 0; j < GAME_BOARD_SIZE_Y; j++)
		{
			line.push_back(L"x");
		}
		gameMap.push_back(line);
	}


	Connection();
	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)updateMapThread, (LPVOID)this, 0, &updateThreadID[threadCounter]);
	threadCounter++;
}


Communication::~Communication()
{
}

vector<vector<tstring>> Communication::getMap(){
	return this->gameMap;
}

void Communication::setGameMap(vector<vector<tstring>> mapAux){

	this->gameMap = mapAux;
}

void Communication::setWindowHandle(HWND handle){

	this->window = handle;
}

HWND Communication::getWindowHandle(){

	return this->window;
}


void Communication::Connection(){

	if (!WaitNamedPipe(PIPE_DUPLEX, NMPWAIT_WAIT_FOREVER)) {
		_tprintf(TEXT("[ERROE] Connecting to the server Pipe\n"), PIPE_DUPLEX);
		exit(-1);
	}

	serverDuplexPipe = CreateFile(PIPE_DUPLEX, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	if (serverDuplexPipe == NULL) {
		_tprintf(TEXT("[ERROR] Connecting pipe '%s'...\n"), PIPE_DUPLEX);
		exit(-1);
	}

	if (!WaitNamedPipe(PIPE_UPDATE, NMPWAIT_WAIT_FOREVER))
	{
		_tprintf(TEXT("[ERROr] Connecting to update pipe:'%s'\n"), PIPE_UPDATE);
		exit(-1);
	}

	serverUpdatePipe = CreateFile(PIPE_UPDATE, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	if (serverUpdatePipe == NULL)
	{
		_tprintf(TEXT("[ERRO] Ligar ao pipe '%s'... (CreateFile)\n"), PIPE_UPDATE);
		exit(-1);
	}
	_tprintf(TEXT("[CLIENT]Connected!\n"));
}


void Communication::sendToServer(tstring msg)
{
	WriteFile(serverDuplexPipe, msg.c_str(), _tcslen(msg.c_str())*sizeof(LPTSTR), &bytesWritten, NULL);
}

void Communication::receiveFromServer()
{
	TCHAR serverAnser[500];
	ReadFile(serverDuplexPipe, serverAnser, sizeof(serverAnser), &bytesRead, NULL);
	serverAnser[bytesRead / sizeof(TCHAR)] = '\0';

	tstring message = serverAnser;
	
}

DWORD WINAPI updateMapThread(LPVOID param){
	
	Communication communication = (*(Communication*)param);
	BOOL isSucceed;
	TCHAR gameMessage[250];

	while (TRUE)
	{
		BOOL isSucceed = ReadFile(serverUpdatePipe, gameMessage, sizeof(gameMessage), &bytesRead, NULL);

		if (!isSucceed || !bytesRead){
			break;
		}

		communication.gameMap.clear();
		vector<tstring> line;
		for (int DWORD = 0; DWORD < sizeof(gameMessage); DWORD++){
			
			if (gameMessage[DWORD] == 'x'){
				line.push_back(TEXT("x"));
			}
			else if (gameMessage[DWORD] == ' '){
				line.push_back(TEXT(" "));
			}
			else if (gameMessage[DWORD] == '*'){
				line.push_back(TEXT("*"));
			}
			else if (gameMessage[DWORD] == 'd'){
				line.push_back(TEXT("d"));
			}
			else if (gameMessage[DWORD] == 'p'){
				line.push_back(TEXT("p"));
			}
			else if (gameMessage[DWORD] == 'b'){
				line.push_back(TEXT("b"));
			}
			else if (gameMessage[DWORD] == 'k'){
				line.push_back(TEXT("k"));
			}
			else if (gameMessage[DWORD] == 'B'){
				line.push_back(TEXT("B"));
			}
			else if (gameMessage[DWORD] == 'k'){
				line.push_back(TEXT("k"));
			}
			else if (gameMessage[DWORD] == '\n'){
				communication.gameMap.push_back(line);
				line.clear();
			}
		}

		isSucceed = InvalidateRect(communication.getWindowHandle(), NULL, TRUE);
	}

	return 0;
}