#pragma once
#include "Element.h"
#include "Util.h"

// Representa um bloco destrutível
// Change this name to Destructible
class Destructive : public Element{
private:

public:
	Destructive(int x,int y);
	~Destructive();

	virtual tstring toString();
};