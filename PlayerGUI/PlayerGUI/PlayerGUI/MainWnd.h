#pragma once
#include <string>
#include <windows.h>
#include "resource.h"
#include "Communication.h"
using namespace std;

#ifdef UNICODE
#define tstring wstring
#else
#define tstring string
#endif

//---------------------------------------------------------------------------
class WWindow
{
	static void drawMap(HWND hWnd);
	static BOOL CALLBACK menuInicial(HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam);
	static LRESULT CALLBACK WndProc(HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam);
	static tstring AppName;
	static bool started;
	static HINSTANCE hInstance;
	static Communication comunnication;
	
	static HBITMAP player;
	static HBITMAP grass;
	static HBITMAP explosion;
	static HBITMAP bomb;
	static HBITMAP blockIndestrutible;
	static HBITMAP blockDestructible;
	static HBITMAP door;
	static HBITMAP key;
	static HBITMAP logo;

public:
	// Utilizaremos o construtor para criar e inicializar
	WWindow(LPCTSTR clsname,LPCTSTR wndname,HWND parent = NULL,	DWORD dStyle = WS_OVERLAPPEDWINDOW | WS_VSCROLL | WS_HSCROLL,
		DWORD dXStyle = 0L,	int x = CW_USEDEFAULT,	int y = CW_USEDEFAULT, int width = CW_USEDEFAULT, int height = CW_USEDEFAULT);

	// Registo da estrutura (WNDCLASSEX) da janela
	bool Register();

	// M�todo para mostrar a janela
	BOOL Show(int dCmdShow = SW_SHOWNORMAL);

	//Uma vez que cada janela � do tipo HWND, utilizaremos 
	//um modo de reconhecer o handle da janela quando utilizado 
	//na aplica��o
	operator HWND();
	
	void StartUp(void);
	
protected:
	//Este ser� o handle global dispon�vel para esta e para outras janelas
	HWND _hwnd;
	//Confirmar fecho da janela
	bool doConfirmation(void);
};
//---------------------------------------------------------------------------