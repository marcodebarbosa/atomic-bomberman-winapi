#include "Util.h"
#define MAX_THREADS 4
#define PIPE_DUPLEX TEXT("\\\\.\\pipe\\server_pipe")
#define PIPE_UPDATE TEXT("\\\\.\\pipe\\server_update") 

// Functions
void Connection();
DWORD  communication();
DWORD WINAPI updateMapThread();

//Global Variables
HANDLE serverDuplexPipe;
HANDLE serverUpdatePipe;
HANDLE updateThread[MAX_THREADS];
DWORD bytesWritten;
DWORD bytesRead;

int _tmain(int argc, LPTSTR argv[]){

#ifdef UNICODE 
	_setmode(_fileno(stdin), _O_WTEXT);
	_setmode(_fileno(stdout), _O_WTEXT);
#endif

	Connection();
	communication();
}

void Connection(){

	if (!WaitNamedPipe(PIPE_DUPLEX, NMPWAIT_WAIT_FOREVER)) {
		_tprintf(TEXT("[ERROE] Connecting to the server Pipe\n"), PIPE_DUPLEX);
		exit(-1);
	}

	serverDuplexPipe = CreateFile(PIPE_DUPLEX, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	
	if (serverDuplexPipe == NULL) {
		_tprintf(TEXT("[ERROR] Connecting pipe '%s'...\n"), PIPE_DUPLEX);
		exit(-1);
	}

	if (!WaitNamedPipe(PIPE_UPDATE, NMPWAIT_WAIT_FOREVER))
	{
		_tprintf(TEXT("[ERROr] Connecting to update pipe:'%s'\n"), PIPE_UPDATE);
		exit(-1);
	}

	serverUpdatePipe = CreateFile(PIPE_UPDATE, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	
	if (serverUpdatePipe == NULL)
	{
		_tprintf(TEXT("[ERRO] Ligar ao pipe '%s'... (CreateFile)\n"), PIPE_UPDATE);
		exit(-1);
	}
	_tprintf(TEXT("[CLIENT]Connected!\n"));
}

DWORD WINAPI updateMapThread(LPVOID param){
	
	BOOL isSucceed;
	TCHAR gameMap[1000];
	
	while (TRUE){
		BOOL isSucceed = ReadFile(serverUpdatePipe, gameMap, sizeof(gameMap), &bytesRead, NULL);

		gameMap[bytesRead / sizeof(TCHAR)] = '\0';

		if (!isSucceed || !bytesRead){
			break;
		}
		_tprintf(TEXT("[CLIENTE] Recebi %d bytes\n"), bytesRead);
		_tprintf(TEXT("%s"),gameMap);

		Sleep(1000);
	}

	return 0;
}

DWORD communication(){

	TCHAR serverRquest[256];
	TCHAR serverAnswer[256];
	TCHAR playerName[256];
	DWORD updateThreadID[MAX_THREADS];
	DWORD threadCounter = 0;
	BOOL isSucceed;
	BOOL nameInputVerification = TRUE;

	_tprintf(TEXT(":: Spectrum Bomberman ::\n"));
	
	do{
		nameInputVerification = TRUE;

		_tprintf(TEXT(":: Player name: "));

		fflush(stdin);
		_fgetts(playerName, 256, stdin);
		playerName[_tcslen(playerName) - 1] = '\0';

		if (wcslen(playerName) == 0){ // String is empty
			_tprintf(TEXT("\nField is empty! Enter your name!\n "));
			nameInputVerification = FALSE;
		}
	} while (nameInputVerification == FALSE);

	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)updateMapThread, NULL, 0, &updateThreadID[threadCounter]);
	threadCounter++;

	_tprintf(TEXT("\n\n:: Spectrum Bomberman ::\n"));
	_tprintf(TEXT("Choose one of the options:\n"));
	_tprintf(TEXT("1.New game\n"));
	_tprintf(TEXT("2.Credits\n"));
	_tprintf(TEXT("3.Quit\n"));

	while (1) {

		//Le da consola
		_tprintf(TEXT("[%s] -> : "),playerName);
		fflush(stdin);

		_fgetts(serverRquest, 256, stdin);
		serverRquest[_tcslen(serverRquest) - 1] = '\0';

		WriteFile(serverDuplexPipe, serverRquest, _tcslen(serverRquest)*sizeof(LPTSTR), &bytesWritten, NULL);

		isSucceed = ReadFile(serverDuplexPipe, serverAnswer, sizeof(serverAnswer), &bytesRead, NULL);

		serverAnswer[bytesRead / sizeof(TCHAR)] = '\0';

		if (!isSucceed || !bytesRead){
			break;
		}

		_tprintf(TEXT("[CLIENTE] Recebi %d bytes: '%s'... (ReadFile)\n"), bytesRead, serverAnswer);
	}

	CloseHandle(serverUpdatePipe);
	CloseHandle(serverDuplexPipe);
	Sleep(200);

	return 0;
}
