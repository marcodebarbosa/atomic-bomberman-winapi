#include "Cell.h"

Cell::Cell(DWORD posX, DWORD posY)
{
	this->posX = posX;
	this->posY = posY;
	this->cellElement = NULL;
}

Cell::~Cell(){

}


void Cell::putElementInCell(Element *newElement){
	this->cellElement = newElement;
}

Element *Cell::getElementInCell(){
	return cellElement;
}