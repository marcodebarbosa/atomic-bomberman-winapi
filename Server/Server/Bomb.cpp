#include "Bomb.h"
#include "Game.h"

DWORD WINAPI bombPlanter(LPVOID param);
BOOL isOutLimits(DWORD posX, DWORD posY);

Bomb::Bomb(Game *game, DWORD playerID, DWORD posX, DWORD posY, DWORD power) :Element(posX, posY)
{
	this->playerID = playerID;
	this->power = power;
	this->game = game;

	CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)bombPlanter, LPVOID(this), 0, NULL);
}

Bomb::~Bomb(){}

Game* Bomb::getGame(){
	return this->game;
}

DWORD Bomb::getPower(){
	return this->power;
}

vector<DWORD> Bomb::getFireBricks(){
	return this->fireBricks;
}

DWORD Bomb::getPlayerID(){
	return this->playerID;
}

DWORD WINAPI bombPlanter(LPVOID param){

	Bomb bomb =(*(Bomb*)param);

	//bomb.getPlayer()->setIsBombActive(TRUE);

	bomb.getGame()->writeMapOnPipe();
	Sleep(2500);
	
	bomb.explosion(&bomb);
	bomb.getGame()->writeMapOnPipe();
	Sleep(1500);

	bomb.getGame()->cleanAfterExplosion(bomb.getFireBricks(), bomb.getPlayerID());
	bomb.getGame()->writeMapOnPipe();
	//bomb.getPlayer()->setIsBombActive(FALSE);

	return 0;
}

BOOL Bomb::explosion(Bomb *bomb){

	DWORD posX = bomb->getPosX();
	DWORD posY = bomb->getPosY();

	BOOL upBrick = TRUE;
	BOOL downBrick = TRUE;
	BOOL leftBrick = TRUE;
	BOOL rightBrick = TRUE;
	
	DWORD explosionPosition;
	for (DWORD i = 0; i <= bomb->power; i++){

		if (upBrick){
			explosionPosition = posX - i;

			if (explosionPosition >= 0){
				if (bomb->game->isDestructible(explosionPosition, posY)){
					game->explodeBrick(explosionPosition, posY);
					addPositionToClean(explosionPosition, posY);
				} else {
					upBrick = FALSE;
				}
			}
		}

		if (downBrick){
			explosionPosition = posX + i;
			if (explosionPosition < 15){
				if (bomb->game->isDestructible(explosionPosition, posY)){
					game->explodeBrick(explosionPosition, posY);
					addPositionToClean(explosionPosition, posY);
				}
				else {
					downBrick = FALSE;
				}
			}
		}

		if (leftBrick){
			explosionPosition = posY - i;
			if (explosionPosition >= 0){
				if (bomb->game->isDestructible(posX, explosionPosition)){
					game->explodeBrick(posX, explosionPosition);
					addPositionToClean(posX, explosionPosition);
				}
				else {
					leftBrick = FALSE;
				}
			}
		}

		if (rightBrick){
			explosionPosition = posY + i;
			if (explosionPosition < 15){
				if (bomb->game->isDestructible(posX, explosionPosition)){
					game->explodeBrick(posX, explosionPosition);
					addPositionToClean(posX, explosionPosition);
				}
				else {
					rightBrick = FALSE;
				}
			}
		}
	}
	return true;
}

void Bomb::addPositionToClean(DWORD posX, DWORD posY){

	this->fireBricks.push_back(posX);
	this->fireBricks.push_back(posY);

}

tstring Bomb::toString(){

	return L"b";
}
