#pragma once
#include "Util.h"
#include "Element.h"

class Player
{
private:
	DWORD id;
	DWORD posX;
	DWORD posY;
	tstring name;
	DWORD lives;
	DWORD points;
	DWORD bombPower;
	BOOL isBombActive;
	HANDLE pipe;

public:

	Player(DWORD playerID, tstring name, HANDLE playerPipe);
	~Player();

	DWORD getPosX();
	DWORD getPosY();
	void setPosX(DWORD posX);
	void setPosY(DWORD posY);

	void setName(tstring name);
	tstring getName();

	void setLives(DWORD lives);
	DWORD getLives()const;

	void setBombPower(DWORD power);
	DWORD getBombPower();

	void setPoints(DWORD points);
	DWORD getPoints();

	void setPipe(HANDLE power);
	HANDLE getPipe();

	void setIsBombActive(BOOL option);
	BOOL getIsBombActive();

	void playerLosesOneLife();

	tstring toString();
};

