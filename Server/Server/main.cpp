#include "Util.h"
#include "Element.h"
#include "Game.h"
#define MAX_CONNECTED 30

#define GAMESTATE_WAITING 1
#define GAMESTATE_PLAYING 2
#define GAMESTATE_END 3

#define PIPE_DUPLEX TEXT("\\\\.\\pipe\\server_pipe") 
#define PIPE_UPDATE TEXT("\\\\.\\pipe\\server_update") 

// Functions
DWORD WINAPI readConsole(LPVOID param);
DWORD WINAPI connectClient(HANDLE hEvent, OVERLAPPED ovl);
BOOL WINAPI sendMapClients();
BOOL WINAPI sendMessageClient(HANDLE client);

// Global variables
DWORD totalConnected = 0;
DWORD GAME_STATE = GAMESTATE_WAITING;
DWORD bytesWritten;
DWORD bytesRead;
BOOL isExit = FALSE;
HANDLE hEvent;
Game *game;

tstring name;

vector <HANDLE> connectedPipes;
vector <HANDLE> inGamePipes;

int _tmain(int argc, LPTSTR argv[]){

	OVERLAPPED ovl;

#ifdef UNICODE 
	_setmode(_fileno(stdin), _O_WTEXT);
	_setmode(_fileno(stdout), _O_WTEXT);
	_setmode(_fileno(stderr), _O_WTEXT);
#endif

	hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	ovl.hEvent = hEvent;
	
	game = new Game();
	
	// Cicle that waits for the client connection
	DWORD i = connectClient(hEvent, ovl);

	_tprintf(TEXT("System is gonna turn off\n"));
	Sleep(10000);
	return 0;
}

DWORD WINAPI connectClient(HANDLE hEvent, OVERLAPPED ovl){

	for (DWORD i = 0; i < MAX_CONNECTED && !isExit; i++){

		connectedPipes.push_back(CreateNamedPipe(PIPE_DUPLEX, PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED, PIPE_WAIT | PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE, MAX_CONNECTED, 256, 256, 1000, NULL));

		if (connectedPipes[connectedPipes.size() - 1] == INVALID_HANDLE_VALUE){

			_tperror(TEXT("Error occurred on the client connection to connectedPipes"));
			exit(-1);
		}

		inGamePipes.push_back(CreateNamedPipe(PIPE_UPDATE, PIPE_ACCESS_OUTBOUND, PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT, 10, sizeof(TCHAR[1000]), sizeof(TCHAR[1000]), 1000, NULL));

		if (inGamePipes[inGamePipes.size() - 1] == INVALID_HANDLE_VALUE){

			_tperror(TEXT("Error occurred on the client connection to inGamePipe"));
			exit(-1);
		}

		_tprintf(TEXT("[SERVER] Waiting for client connection\n"));
		ConnectNamedPipe(connectedPipes.at(i), &ovl);
		WaitForSingleObject(ovl.hEvent, INFINITE);

		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)readConsole, (LPVOID)totalConnected, 0, NULL);
		totalConnected++;
	}

	return 0;
}

BOOL WINAPI sendMapClients(){

	//for (DWORD i = 1; inGamePipes.size(); i++){
	//Poder� dar stress no bytesWritten, � ver
	WriteFile(inGamePipes[0], game->packMapToSend().c_str(), _tcslen(game->packMapToSend().c_str())*sizeof(TCHAR), &bytesWritten, NULL);
	//}
	return TRUE;
}

BOOL WINAPI sendMessageClient(HANDLE client){

	//WriteFile(PIPE_EXAMPLE, game->packMapToSend().c_str(), _tcslen(game->packMapToSend().c_str())*sizeof(TCHAR), &bytesWritten, NULL);

	return TRUE;
}

tstring insertOnRegistry(DWORD playerID){
	
	HKEY key;
	DWORD state;
	DWORD version;
	DWORD size = 20;
	tstring playerName = game->getPlayer(playerID)->getName();
	tstring pathToRegister = TEXT("Software\\SpectumBomberman\\") + playerName;

	//Create or opening the key
	if (RegCreateKeyEx(HKEY_CURRENT_USER, pathToRegister.c_str(), 0, NULL,
		REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &key, &state) != ERROR_SUCCESS)
	{
		return TEXT("Error opening the key");
	}
	else
	{
		//Initialization of the values
		if (state == REG_CREATED_NEW_KEY)
		{
			tcout << TEXT("Key: HKEY_CURRENT_USER\\Software\\SpectumBomberman\\") + playerName + TEXT(" created\n");
			RegSetValueEx(key, TEXT("Name"), 0, REG_SZ, (LPBYTE)playerName.c_str(), _tcslen(playerName.c_str())*sizeof(TCHAR));
			DWORD points = game->getPlayer(playerID)->getPoints();
			RegSetValueEx(key, TEXT("Points"), 0, REG_DWORD, (LPBYTE)&points, sizeof(DWORD));
			tcout << (TEXT("All values saved\n"));
			return TEXT("All values saved");
		}
		//If exists, read the values
		else if (state == REG_OPENED_EXISTING_KEY)
		{
			DWORD playerPoints = 1;
			tstring playerName;

			tcout << TEXT("Key: HKEY_CURRENT_USER\\Software\\SpectumBomberman\\") + playerName + TEXT(" opened\n");
			RegQueryValueEx(key, TEXT("Name"), NULL, NULL, (LPBYTE)playerName.c_str(), &size);
			size = sizeof(version);
			RegQueryValueEx(key, TEXT("Points"), NULL, NULL, (LPBYTE)&playerPoints, &size);
			
			game->getPlayer(playerID)->setPoints(playerPoints);
			tcout << TEXT("\nName read:  " << game->getPlayer(playerID)->getName());
			tcout << TEXT("\nPoints read:  " << playerPoints);
			return TEXT("Your data was loaded with success!\n");
		}
		RegCloseKey(key);
	}
}

DWORD WINAPI readConsole(LPVOID param){

	DWORD playerID = (DWORD)param;
	TCHAR buf[256];
	HANDLE client = connectedPipes[(int)param];
	BOOL isSucceed;

	do{
		//Ler do pipe
		isSucceed = ReadFile(client, buf, sizeof(buf), &bytesRead, NULL);

		if (!isSucceed || !bytesRead){
			break;
		}

		buf[bytesRead / sizeof(LPTSTR)] = '\0';
		tstring request = buf;

		tcout << TEXT("\nPlayer ") << playerID << TEXT(" request :") << buf;

		tistringstream iss(request);
		tstring command;
		iss >> command;
		
		if (command == TEXT("join"))
			iss >> name;

		tstring answer;
		switch (GAME_STATE)
		{
		case GAMESTATE_WAITING:

			if (command == TEXT("join")){
				
				game->addPlayer(playerID, name, inGamePipes[playerID]);
				answer = insertOnRegistry(playerID);
			}
			else if (request == TEXT("newgame")){
				game->buildGameboard();

				//game->createEnimy();

				game->setGameStarted(TRUE);
				sendMapClients();
				GAME_STATE = GAMESTATE_PLAYING;
				answer = TEXT("New game being created. Wait a moment");
			}
			else {
				answer = TEXT("Comand not recognized");
			}
			break;

		case GAMESTATE_PLAYING:

			if (request == TEXT("left")){
				answer = game->movePlayerLeft(playerID);
				sendMapClients();
			}
			else if (request == TEXT("right")){
				answer = game->movePlayerRight(playerID);
				sendMapClients();
			}
			else if (request == TEXT("up")){
				answer = game->movePlayerUp(playerID);
				sendMapClients();
			}
			else if (request == TEXT("down")){
				answer = game->movePlayerDown(playerID);
				sendMapClients();
			}
			else if (request == TEXT("bomb")){
				game->placeBomb(playerID);
				answer = TEXT("The bomb has been planted!");
			}
			else if (request == TEXT("exit")){
				answer = TEXT("Exiting game");
				exit(0);
			}
			else {
				answer = TEXT("Comand not recognized");
			}
			break;

		case GAMESTATE_END:
			break;
		}

		if (!WriteFile(client, answer.c_str(), _tcslen(answer.c_str())*sizeof(LPTSTR), &bytesWritten, NULL)) {
			_tperror(TEXT("[ERRO] Escrever no pipe... (WriteFile)\n"));
			exit(-1);
		}

	} while (_tcsncmp(buf, TEXT("quit"), 3));

	isExit = TRUE;
	SetEvent(hEvent);

	tcout << "Client " << playerID << " disconnected" << endl;

	if (!DisconnectNamedPipe(client)){
		_tperror(TEXT("Server turning off the Pipe!"));
		exit(-1);

		Sleep(5000);
		CloseHandle(client);
	}
	return 0;
}