#pragma once
#include "Util.h"
#include "Destructive.h"
#include "Indestructible.h"
#include "Empty.h"
#include "Player.h"
#include "ExtraLife.h"
#include "ExtraBomb.h"
#include "Bomb.h"
#include "Cell.h"
#include "Explosion.h"

class Game{

private:
	vector<vector<Cell>> gameMap;
	vector <Player*> player;
	BOOL isGameStarted;
	DWORD mapHeight;
	DWORD mapWidth;

public:
	Game();
	~Game();
	
	// Map construction
	DWORD generateRandomNumber(DWORD max, DWORD min);
	void buildGameboard();
	void setGameStarted(BOOL state);
	BOOL getGameStarted();
	void setGameMapHeight(DWORD length);
	DWORD getGameMapHeight()const;
	void setGameMapWidth(DWORD width);
	DWORD getGameMapWidth()const;
	vector<vector<Cell>> getGameMap();

	// Player Actions
	void addPlayer(DWORD clientID, tstring name, HANDLE playerPipe);
	Player *getPlayer(DWORD playerID);
	tstring movePlayerLeft(DWORD playerID);
	tstring movePlayerRight(DWORD playerID);
	tstring movePlayerDown(DWORD playerID);
	tstring movePlayerUp(DWORD playerID);
	BOOL isOutLimits(DWORD posX, DWORD posY);

	// Bomb
	BOOL isDestructible(DWORD posX, DWORD posY);
	void explodeBrick(DWORD posX, DWORD posY);
	void printExplosion(DWORD posX, DWORD posY);
	void cleanAfterExplosion(vector<DWORD> firebricks, DWORD playerID);
	void placeBomb(DWORD playerID);

	// Enemy
	void createEnimy();
	void writeMapOnMappedFile();

	BOOL WINAPI Game::sendMapClients();
	BOOL checkOccupation(DWORD x, DWORD y);

	// Print/Communication
	BOOL WINAPI Game::writeMapOnPipe();
	tstring packMapToSend();
	void printGameBoard();
};