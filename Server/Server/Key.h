#pragma once
#include "Element.h"
#include "Util.h"

class Key :public Element
{
public:
	Key(DWORD posX, DWORD posY);
	~Key();

	virtual tstring toString();
};

