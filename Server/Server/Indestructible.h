#pragma once
#include "Element.h"
#include "Util.h"

class Indestructible :public Element{
private:

public:
	Indestructible(int x, int y);
	~Indestructible();

	virtual tstring toString();
};