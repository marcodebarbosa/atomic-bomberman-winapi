//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by PlayerGUI.rc
//
#define IDD_DIALOG1                     101
#define IDB_GRASS                       110
#define IDB_PLAYER                      111
#define IDB_INDESTRUCTIBLE              113
#define IDB_DESTRUCTIBLE                114
#define IDB_DOOR                        115
#define IDB_BOMB                        117
#define IDB_BITMAP1                     118
#define IDB_EXPLOSION                   118
#define IDB_BITMAP2                     119
#define IDC_NOME                        1001
#define IDC_REGISTA                     1002
#define IDC_EDIT1                       1003

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        120
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
