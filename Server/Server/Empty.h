#pragma once
#include "Element.h"
#include "Util.h"

class Empty: public Element{
private:

public:
	Empty(int x,int y);
	~Empty();

	virtual tstring toString();
};

