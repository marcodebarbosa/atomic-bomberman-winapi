#include "MainWnd.h"

bool WWindow::started = false;
tstring WWindow::AppName;
HINSTANCE WWindow::hInstance = NULL;
Communication WWindow::comunnication;

HBITMAP WWindow::player;
HBITMAP WWindow::grass;
HBITMAP WWindow::explosion;
HBITMAP WWindow::bomb;
HBITMAP WWindow::blockIndestrutible;
HBITMAP WWindow::blockDestructible;
HBITMAP WWindow::door;
HBITMAP WWindow::key;
HBITMAP WWindow::logo;

//---------------------------------------------------------------------------
WWindow::WWindow(LPCTSTR clsname, LPCTSTR wndname,	HWND parent,	DWORD dStyle,	DWORD dXStyle,	int x,	int y,	int width,	int height)
{	
	AppName = clsname;
	//Registar a classe se ainda n�o foi registada antes
	if (!started)
		StartUp();

	// Criar uma nova janela
	_hwnd = CreateWindowEx(dXStyle, clsname, wndname, dStyle, x, y, width,
		height, parent, NULL, hInstance, NULL);

	// Se a janela n�o foi criada terminar o programa!
	if (_hwnd == NULL){
		MessageBox(NULL, TEXT("na criacao da janela"), TEXT("Erro"), MB_OK | MB_ICONERROR);
		exit(1);
	}	
	//Armazenar o ponteiro this na zona cbClsExtra da estrutura WNDCLASSEX
	SetWindowLongPtr(_hwnd, 0, (long) this);
}
//---------------------------------------------------------------------------

bool WWindow::Register(){
	WNDCLASSEX _WndClsEx;

	//Defini��o das caracter�sticas da janela "_WndClsEx"
	_WndClsEx.cbSize = sizeof(WNDCLASSEX);
	_WndClsEx.style = CS_HREDRAW|CS_VREDRAW;
	_WndClsEx.lpfnWndProc = WndProc;	//Fun��o Membro
	_WndClsEx.cbClsExtra = 0;
	_WndClsEx.cbWndExtra = sizeof(WWindow *);
	_WndClsEx.hInstance = GetModuleHandle(AppName.c_str());
	_WndClsEx.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	_WndClsEx.hCursor = LoadCursor(NULL, IDC_ARROW);
	_WndClsEx.hbrBackground = static_cast<HBRUSH>(GetStockObject(WHITE_BRUSH));
	_WndClsEx.lpszMenuName = NULL;
	_WndClsEx.lpszClassName = AppName.c_str();
	_WndClsEx.hIconSm = LoadIcon(NULL, IDI_INFORMATION);

	if (!RegisterClassEx(&_WndClsEx)){
		MessageBox(NULL,TEXT("no registo da classe"),TEXT("Erro"), MB_OK|MB_ICONERROR);
		return false;
	}
	return true;
}

//---------------------------------------------------------------------------
BOOL WWindow::Show(int dCmdShow)
{
	DialogBox(hInstance, (TCHAR*)IDD_DIALOG1,_hwnd, menuInicial);

	// ============================================================================
	// Mostrar a janela
	// ============================================================================
	if (!ShowWindow(_hwnd, dCmdShow))	// "hWnd"= handler da janela, devolvido 
		return FALSE;					// por "CreateWindow"; "dCmdShow"= modo de
										// exibi��o (p.e. normal, modal); � passado
										// como par�metro de WinMain()

	if (!UpdateWindow(_hwnd))			// Refrescar a janela (Windows envia � janela
		return FALSE;					// uma mensagem para pintar, mostrar dados,
										// (refrescar), etc)
	return TRUE;
}
//---------------------------------------------------------------------------
//Confirmar fecho da janela
bool WWindow::doConfirmation(void){
	if (MessageBox(_hwnd,  TEXT("Are you sure?"), TEXT("Exit"),MB_ICONQUESTION|MB_YESNO) == IDYES)
		return true;
	return false;
}
//---------------------------------------------------------------------------
WWindow::operator HWND()
{
	//Uma vez que cada janela � do tipo HWND, utilizaremos 
	//um modo de reconhecer o handle da janela quando utilizado 
	//na aplica��o
	return _hwnd;
}
//---------------------------------------------------------------------------
void WWindow::StartUp(void){
	hInstance = GetModuleHandle(AppName.c_str());
	if (Register())
		started = true;
	
}
//---------------------------------------------------------------------------
LRESULT WWindow::WndProc(HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam){
	
	//Saber sobre que janela estamos a trabalhar
	WWindow *pWin = (WWindow *) GetWindowLongPtr(hWnd, 0);
	TCHAR str[100];
	BOOL eRato = FALSE;

	comunnication.setWindowHandle(hWnd);

	switch (messg)  {

		case WM_CREATE:

			player = LoadBitmap(hInstance, (TCHAR*) IDB_PLAYER);
			grass = LoadBitmap(hInstance, (TCHAR*) IDB_GRASS);
			explosion = LoadBitmap(hInstance, (TCHAR*) IDB_EXPLOSION);
			blockDestructible = LoadBitmap(hInstance, (TCHAR*) IDB_DESTRUCTIBLE);
			blockIndestrutible = LoadBitmap(hInstance, (TCHAR*) IDB_INDESTRUCTIBLE);
			bomb = LoadBitmap(hInstance, (TCHAR*)IDB_BOMB);
			door = LoadBitmap(hInstance, (TCHAR*)IDB_DOOR);
			break;

		case WM_CLOSE:	// Destruir a janela e terminar o programa
			// "PostQuitMessage(Exit Status)"
			if (pWin->doConfirmation())
				PostQuitMessage(0);
			break;

		case WM_KEYDOWN:
			if (wParam == VK_UP)
			{
				comunnication.sendToServer(L"up");
				comunnication.receiveFromServer();
			
			}
			else if (wParam == VK_DOWN)
			{
				comunnication.sendToServer(L"down");
				comunnication.receiveFromServer();
			}
			else if (wParam == VK_LEFT)
			{
				comunnication.sendToServer(L"left");
				comunnication.receiveFromServer();
			}
			else if (wParam == VK_RIGHT)
			{
				comunnication.sendToServer(L"right");
				comunnication.receiveFromServer();
			}
			else if (wParam == VK_SPACE)
			{
				comunnication.sendToServer(L"bomb");
				comunnication.receiveFromServer();
			}
		
			InvalidateRect(hWnd, NULL, 1);

			break;
		case WM_PAINT:
			drawMap(hWnd);
			break;
		default:

		// Neste exemplo, para qualquer outra mensagem (p.e. "minimizar", "maximizar",
		// "restaurar") n�o � efectuado nenhum processamento, apenas se segue 
		// o "default" do Windows DefWindowProc()
		return(DefWindowProc(hWnd, messg, wParam, lParam));
		break;
	}
	return(0);
}


void WWindow::drawMap(HWND hWnd)
{
	vector<vector<tstring>> map = comunnication.getMap();
	
	PAINTSTRUCT area;
	HDC hdc, auxdc;

	hdc = BeginPaint(hWnd, &area);
	auxdc = CreateCompatibleDC(hdc);

	for (DWORD j = 0; j < GAME_BOARD_SIZE_X; j++)
	{
		for (DWORD i = 0; i < GAME_BOARD_SIZE_Y; i++)
		{
			if (map[i][j] == L"x"){
				SelectObject(auxdc, blockIndestrutible);
			} else if (map[i][j] == L" "){
				SelectObject(auxdc, grass);
			} else if (map[i][j] == L"d"){
				SelectObject(auxdc, blockDestructible);
			} else if (map[i][j] == L"p"){
				SelectObject(auxdc, player);
			} else if (map[i][j] == L"b"){
				SelectObject(auxdc, bomb);
			} else if (map[i][j] == L"b"){
				SelectObject(auxdc, bomb);
			} else if (map[i][j] == L"*"){
				SelectObject(auxdc, explosion);
			}
				
			BitBlt(hdc, 20 + j * 72, 20 + i * 72, 72, 72, auxdc, 0, 0, SRCCOPY);
		}
	}
	
	DeleteDC(auxdc);
	EndPaint(hWnd, &area);
}

BOOL WWindow::menuInicial(HWND hWnd, UINT messg, WPARAM wParam, LPARAM lParam)
{
	switch (messg)
	{
	case WM_CLOSE:	// Destruir a janela e terminar o programa

		PostQuitMessage(0);
		break;
	case WM_INITDIALOG: // quando o dialogo � criado, entra aqui
		break;

	case WM_COMMAND:	// foi usado num controlo
		switch (wParam)
		{
		case IDOK: // come�ar jogo
			comunnication.sendToServer(L"newgame");
			EndDialog(hWnd, MB_OK);
			break;
		case IDCANCEL:
			PostQuitMessage(0);
			break;
		case IDC_REGISTA:
			TCHAR name[20];
			tstring message = L"join ";
			GetDlgItemText(hWnd, IDC_NOME, name, sizeof(name));
			message += name;
			comunnication.sendToServer(message);
			
			break;
		}
		break;

	default:
		;
	}
	return false;
}