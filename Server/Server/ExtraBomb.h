#pragma once
#include "Element.h"
#include "Util.h"

using namespace std;

class ExtraBomb :public Element{
private:

public:
	ExtraBomb(DWORD posX, DWORD posY);
	ExtraBomb(int x,int y);
	~ExtraBomb();

	virtual tstring toString();
};