#pragma once
#include "Element.h"
#include "Util.h"

class ExtraLife :public Element{
private:

public:
	ExtraLife(DWORD posX, DWORD posY);
	ExtraLife(int x,int y);
	~ExtraLife();

	tstring toString();
};