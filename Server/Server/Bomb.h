#pragma once
#include "Util.h"
#include "Element.h"
class Game;

class Bomb :public Element{

private:
	DWORD playerID;
	DWORD power;
	Game *game;
	vector<DWORD> fireBricks;

public:
	Bomb(Game *game, DWORD playerID, DWORD posX, DWORD posY, DWORD power);
	~Bomb();

	DWORD getPower();
	DWORD getPlayerID();
	Game* getGame();
	vector<DWORD> getFireBricks();
	virtual tstring toString();

	// Functions
	BOOL explosion(Bomb *bomb);
	void addPositionToClean(DWORD posX, DWORD posY);
	BOOL cleanExplosion(Bomb *bomb);
};