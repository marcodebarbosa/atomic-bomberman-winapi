#include "Util.h"
#define MAX_THREADS 4
#define PIPE_DUPLEX_ENIMY TEXT("\\\\.\\pipe\\enimy_pipe") 
#define MAPPED_FILE_NAME  TEXT("enemyMappedFile")
#define WRITE_SEMAPHORE_NAME TEXT("semaphoreWrite")

// Functions
void Connection();
DWORD  communication();
DWORD WINAPI updateMapThread();

//Global Variables
HANDLE enemyDuplexPipe;

HANDLE updateThread[MAX_THREADS];
DWORD bytesWritten;
DWORD bytesRead;

// Mapped files
OVERLAPPED ovl3;
HANDLE hEvent3, Semaforo;
HANDLE sharedMemory;
LPCTSTR memoryPointer;

int _tmain(int argc, LPTSTR argv[]){

#ifdef UNICODE 
	_setmode(_fileno(stdin), _O_WTEXT);
	_setmode(_fileno(stdout), _O_WTEXT);
#endif

	Connection();
	communication();
}

void Connection(){

	if (!WaitNamedPipe(PIPE_DUPLEX_ENIMY, NMPWAIT_WAIT_FOREVER)) {
		_tprintf(TEXT("[ERROE] Connecting to the server Pipe\n"), PIPE_DUPLEX_ENIMY);
		exit(-1);
	}

	enemyDuplexPipe = CreateFile(PIPE_DUPLEX_ENIMY, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	
	if (enemyDuplexPipe == NULL) {
		_tprintf(TEXT("[ERROR] Connecting pipe '%s'...\n"), PIPE_DUPLEX_ENIMY);
		exit(-1);
	}
}

DWORD WINAPI updateMapThread(LPVOID param){
	
	BOOL isSucceed;
	TCHAR gameMap[1000];
	TCHAR NomeMemoria[] = TEXT("Nome da Mem�ria Partilhada");
	
	Semaforo = CreateSemaphore(NULL, 1, 1, WRITE_SEMAPHORE_NAME);
	sharedMemory = CreateFileMapping((HANDLE)0xFFFFFFFF, NULL, PAGE_READWRITE, 0, sizeof(gameMap), NomeMemoria);

	if (Semaforo == NULL || sharedMemory == NULL){
		_tprintf(TEXT("[Erro]Cria��o de objectos do Windows(%d)\n"), GetLastError());
		return -1;
	}

	sharedMemory = (tstring*)MapViewOfFile(sharedMemory, FILE_MAP_WRITE, 0, 0, sizeof(500));

	if (sharedMemory == NULL){
		_tprintf(TEXT("[Erro]Mapeamento da mem�ria partilhada(%d)\n"), GetLastError());
		return -1;
	}


	while (TRUE){
		//BOOL isSucceed = ReadFile(serverUpdatePipe, gameMap, sizeof(gameMap), &bytesRead, NULL);

		//gameMap[bytesRead / sizeof(TCHAR)] = '\0';

		//if (!isSucceed || !bytesRead){
		//	break;
		//}
		//_tprintf(TEXT("[CLIENTE] Recebi %d bytes\n"), bytesRead);
		//_tprintf(TEXT("%s"),gameMap);

		//DIFERE

		Sleep(1000);
	}

	return 0;
}

DWORD communication(){

	TCHAR serverRquest[256];
	TCHAR serverAnswer[256];
	TCHAR playerName[256];
	DWORD updateThreadID[MAX_THREADS];
	DWORD threadCounter = 0;
	BOOL isSucceed;
	BOOL nameInputVerification = TRUE;

	//CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)updateMapThread, NULL, 0, &updateThreadID[threadCounter]);
	//threadCounter++;

	while (1) {

		tstring serverRequest = L"enemy";

		tcout << "ACEDEU AO BICHO";

		WriteFile(enemyDuplexPipe, serverRquest, _tcslen(serverRquest)*sizeof(LPTSTR), &bytesWritten, NULL);

		isSucceed = ReadFile(enemyDuplexPipe, serverAnswer, sizeof(serverAnswer), &bytesRead, NULL);

		serverAnswer[bytesRead / sizeof(TCHAR)] = '\0';

		if (!isSucceed || !bytesRead){
			break;
		}

		_tprintf(TEXT("[CLIENTE] Recebi %d bytes: '%s'... (ReadFile)\n"), bytesRead, serverAnswer);
	}

	CloseHandle(enemyDuplexPipe);
	CloseHandle(enemyDuplexPipe);
	Sleep(200);

	return 0;
}
