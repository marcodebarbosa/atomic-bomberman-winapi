#include <windows.h>
#include <tchar.h>
#include <io.h>
#include <fcntl.h>
#include <stdio.h>
#include <atlconv.h>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#define GAME_BOARD_SIZE_X 15
#define GAME_BOARD_SIZE_Y 15

using namespace std;

//Permitir que o mesmo c�digo possa funcionar para ASCII ou UNICODE
#ifdef UNICODE
#define tcout wcout
#define tcin wcin
#define tcerr wcerr
#define tstring wstring
#define tfstream wifstream
#define tos wostringstream
#define tostream wostream
#define tstringstream wstringstream
#define tistringstream wistringstream
#define tifastream wifastream
#else
#define tcout cout
#define tcin cin
#define tcerr cerr
#define tstring string
#define tfstream ifstream
#define tos ostringstream
#define tostream ostream
#define tstringstream stringstream
#define tistringstream istringstream
#define tifastream ifastream
#endif