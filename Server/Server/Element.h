#pragma once
#include "Util.h"

class Element {

private:
	DWORD posX;
	DWORD posY;

public:
	Element(DWORD posX, DWORD posY);
	~Element();

	DWORD getPosX();
	DWORD getPosY();

	void setPosX(DWORD x);
	void setPosY(DWORD y);

	virtual tstring toString() = 0;
};

